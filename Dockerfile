FROM php:8.3-fpm-alpine AS base

# Set variable
ENV APP_ROOT '/srv'

# Set working directory
WORKDIR ${APP_ROOT}

# Install dependencies servers
RUN apk --update --no-cache add build-base \
  postgresql-dev \
  libzip-dev \
  linux-headers \
  zip

# Install extensions
RUN docker-php-ext-install pdo \
  pdo_pgsql \
  zip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy existing application directory contents
ADD . ${APP_ROOT}

# Add full permissions directory storage
RUN chmod -R 777 ${APP_ROOT}/storage

FROM base AS dev
# Set variable
ENV APP_ENV development
ENV APP_DEBUG true

RUN apk --update --no-cache add nano

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
CMD php -S 0.0.0.0:9000 -t public

FROM base AS prod
# Set variable
ENV APP_ENV production
ENV APP_DEBUG false

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
CMD ["php-fpm"]
