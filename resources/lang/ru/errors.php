<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'critical_error' => 'Критическая ошибка сервера',
    'unauthenticated' => 'Вы не авторизованы для этого запроса',
    'unauthorized' => 'У вас нет прав для этого запроса',
    'page_not_found' => 'Запрошенная страница не существует',
    'resource_not_found' => 'Запрошенный ресурс не найден',
    'resource_not_permissions' => 'У вас не достаточно прав',
    'resource_forbidden' => 'Доступ запрещен',
    'resource_conflict' => 'Запрос не может быть выполнен из-за конфликта с текущим состоянием ресурса',
    'relation_not_found' => 'Запрашиваемая связь не существует',
    'validation_failed' => 'Ошибка при проверки переданных данных',
    'record_exist' => 'Данная запись уже существует',
    'record_create_error' => 'Ошибка создании записи',
    'record_conflict' => 'Конфликт с текущем состоянием ресурса',
    'user_not_found' => 'Такого пользователя не существует',
    'user_exist' => 'Такого пользователя не существует',
    'user_create_error' => 'Ошибка при создании пользователя',
    'password_failed' => 'Пароль введен не верно',

];
