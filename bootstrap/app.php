<?php
use App\Exceptions\ResponderException;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        channels: __DIR__.'/../routes/channels.php',
        health: '/up'
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->alias([
            'user_role' => \App\Http\Middleware\UserRole::class,
        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        // * For Responder
        $exceptions->render(function (Exception $e, Request $request) {
            return (new ResponderException())->render($request, $e);
        });
    })
    ->create();
