<?php
declare(strict_types=1);
namespace App\Actions;

use Illuminate\Support\Collection;
use App\Interfaces\ActionInterface;

abstract class BaseAction implements ActionInterface
{
    protected const string UNAUTHENTICATED = 'unauthenticated';
    protected const string RECORD_EXIST = 'record_exist';
    protected const string RECORD_CONFLICT = 'record_conflict';
    protected const string USER_NOT_FOUND = 'user_not_found';
    protected const string PASSWORD_FAILED = 'password_failed';
    protected const string CRITICAL_ERROR = 'critical_error';

    /**
     * [Сервисная бизнес-логика]
     * @var $service
     */
    protected mixed $service;

    /**
     * [Репозиторий для работы с определенной моделью]
     * @var $repository
     */
    protected mixed $repository;

    /**
     * [$isFail description] - Статус ошибки выполнения логики Action
     * @var bool|null $isFail
     */
    protected bool $isFail = false;

    /**
     * [Главные данные, которые вставляются в $errorData]
     * @var Collection|array|null $errorData
     */
    protected Collection|array|null $errorData = null;

    /**
     * [Дополнительные данные, которые вставляются помимо даты]
     * @var array $meta
     */
    protected array $meta = [];

    /**
     * [Статус возвращаемого ответа]
     * @var int|null $status
     */
    protected int|null $status = null;

    protected string|null $errorCode = null;
    protected string|null $errorMessage = null;

    /**
     * @param string $errorCode
     * @param string|null $errorMessage
     * @param int|null $status
     * @param array|null $data
     * @return false
     */
    protected function error(string $errorCode, string|null $errorMessage = null, int|null $status = null, array|null $data = null): false
    {
        $this->isFail = true;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->status = $status;
        $this->errorData = $data;

        return false;
    }

    /**
     * @return Collection|array|null
     */
    public function getErrorData(): Collection|array|null
    {
        return $this->errorData;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @return string|null
     */
    public function getErrorMessage(): string|null
    {
        return $this->errorMessage;
    }

    /**
     * @return string|null
     */
    public function getErrorCode(): string|null
    {
        return $this->errorCode;
    }

    /**
     * @return bool
     */
    public function isFail(): bool
    {
        return $this->isFail;
    }

    public function getStatus(): int|bool|null
    {
        return $this->status;
    }

    public function getLimit(array $data, int $initLimit = 10): int
    {
        return isset($data['limit']) ? (int)$data['limit'] : $initLimit;
    }

    public function getOffset(array $data, int $initOffset = 0): int
    {
        return isset($data['offset']) ? (int)$data['offset'] : $initOffset;
    }

//    /**
//     * @param array $data
//     * @return Collection|array|bool
//     */
//    public abstract function handle(mixed $data): Collection|array|bool;
}
