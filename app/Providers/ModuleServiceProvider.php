<?php

namespace App\Providers;

use Database\Seeders\DatabaseSeeder;
use Eloquent;
use Exception;
use File;
use Illuminate\Support\ServiceProvider;
use Route;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

class ModuleServiceProvider extends ServiceProvider
{
    private string|null $modulesBasePath = null;
    private string $pathControllers = "Domain/Controllers";
    private string $pathRoutes = "Domain/Routes";
    private string $pathMigrations = "Domain/Database/Migrations";
    private string $pathConfigs = "Domain/Configs";
    private string $pathSeeders = "Domain/Database/Seeders";

    /**
     * Register services.
     */
    public function register(): void
    {
        $this->modulesBasePath = base_path('modules');
    }

    /**
     * Bootstrap services.
     * @throws Exception
     */
    public function boot(): void
    {
        foreach(File::directories($this->modulesBasePath) as $moduleDirectory)
        {
            $moduleName = basename($moduleDirectory);

            $this->registerRoutes($moduleName);
            $this->registerMigrations($moduleName);
            $this->registerConfig($moduleName);
            // $this->registerSeeders($moduleName);
        }

        $this->registerFactories();
    }

    private function registerRoutes(string $moduleName): void
    {
        if(file_exists("$this->modulesBasePath/$moduleName/$this->pathRoutes/api.php"))
            Route::middleware('api')
                ->prefix('api')
                ->group("$this->modulesBasePath/$moduleName/$this->pathRoutes/api.php");

        if(file_exists("$this->modulesBasePath/$moduleName/$this->pathRoutes/console.php"))
            Route::middleware('console')
                ->group("$this->modulesBasePath/$moduleName/$this->pathRoutes/console.php");
    }

    private function registerMigrations(string $moduleName): void
    {
        if(file_exists("$this->modulesBasePath/$moduleName/$this->pathMigrations"))
            $this->loadMigrationsFrom("$this->modulesBasePath/$moduleName/$this->pathMigrations");
    }

    /**
     * @throws Exception
     */
    private function registerConfig(string $moduleName): void
    {
        if(file_exists("$this->modulesBasePath/$moduleName/$this->pathConfigs")){
            foreach(File::files("$this->modulesBasePath/$moduleName/$this->pathConfigs") as $file){
                $path = $file->getPath();
                $name = $file->getBasename();

                if(!is_array(require "$path/$name"))
                    throw new Exception("Конфигурация $name должна быть массивом");

                $this->mergeConfigFrom("$path/$name", strtolower($moduleName));
            }
        }
    }

    private function registerFactories(): void
    {
        Factory::guessFactoryNamesUsing(function (string $model_name) {
            $factoryClass = null;

            $model_name = Str::afterLast($model_name, '\\');

            // Попытка найти в модулях
            foreach(File::directories($this->modulesBasePath) as $moduleDirectory)
            {
                $moduleName = basename($moduleDirectory);

                $namespace = "Modules\\$moduleName\\Domain\\Database\\Factories\\";
                $factory = $namespace . Str::afterLast($model_name, '\\') . 'Factory';
                $factoryFile = base_path('modules/'.str_replace('\\', '/', Str::replaceFirst("Modules\\", '', "$factory.php")));

                if(file_exists($factoryFile)){
                    $factoryClass = $factory;
                    break;
                }
            }

            // Поиск в стандартных laravel путях
            if(!$factoryClass){
                $factoryClass = "Database\\Factories\\{$model_name}Factory";
            }


            return $factoryClass;
        });
    }

    private function registerSeeders(string $moduleName): void
    {

        if(file_exists("$this->modulesBasePath/$moduleName/$this->pathSeeders")){
            foreach(File::files("$this->modulesBasePath/$moduleName/$this->pathSeeders") as $file){
                $fileName = $file->getBasename();
                DatabaseSeeder::call("$this->modulesBasePath/$moduleName/$this->pathSeeders/$fileName");
            }
        }
    }

}
