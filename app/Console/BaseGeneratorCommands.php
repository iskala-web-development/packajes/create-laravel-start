<?php
declare(strict_types=1);
namespace App\Console;

use Error;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class BaseGeneratorCommands extends GeneratorCommand
{
    protected string|null $prefixName = null;
    protected string|null $rootNamespace = null;
    protected string|null $path = null;

    public function handle(): void
    {
        $this->beforeHandle();

        $this->path = $this->rootPath();

        if(!$this->rootNamespace)
            throw new Error('Укажите $rootNamespace');

        if($this->prefixName)
            $this->input->setArgument('name', $this->argument('name').$this->prefixName);

        parent::handle();
    }

    protected function beforeHandle()
    {
        //
    }

    protected function getPath($name): string
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        $name = Str::replaceFirst("\\", '', $name);

        return base_path($this->path).'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace) : string {
        $moduleName = $this->argument('moduleName');

        return "$rootNamespace/$moduleName/$this->rootNamespace";
    }

    protected function rootPath(): string
    {
        return config('module.rootDir');
    }

    protected function rootNamespace(): string
    {
        return config('module.rootNamespace');
    }

    protected function getStub()
    {
        // TODO: Implement getStub() method.
    }
}
