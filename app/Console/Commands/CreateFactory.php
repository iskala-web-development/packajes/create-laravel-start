<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Console\BaseGeneratorCommands;

class CreateFactory extends BaseGeneratorCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:factory {name} {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Factory';

    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected $type = 'Factory'; // shows up in console

    protected string|null $rootNamespace = 'Domain/Database/Factories';

    protected string|null $prefixName = 'Factory';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return base_path('stubs/factory.stub');
    }
}
