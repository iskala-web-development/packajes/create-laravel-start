<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Console\BaseGeneratorCommands;

class CreateAction extends BaseGeneratorCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:action {name} {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Action';

    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected $type = 'Action'; // shows up in console

    protected string|null $rootNamespace = 'Actions';

    protected string|null $prefixName = 'Action';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return base_path('stubs/action.stub');
    }
}
