<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:migration {name} {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Migration';

    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected string $type = 'Migration'; // shows up in console

    public function handle(): void
    {
        $name = $this->argument('name');
        $moduleName = $this->argument('moduleName');

        $this->runCommand("make:migration", ['name' => $name, '--path' => config('module.rootDir')."/$moduleName/Domain/Database/Migrations"], $this->output);
    }
}
