<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Console\BaseGeneratorCommands;

class CreateModel extends BaseGeneratorCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:model {name} {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Model';

    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected $type = 'Model'; // shows up in console

    protected string|null $rootNamespace = 'Domain\Models';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return base_path('stubs/model.stub');
    }
}
