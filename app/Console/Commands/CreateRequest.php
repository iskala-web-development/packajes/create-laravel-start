<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Console\BaseGeneratorCommands;

class CreateRequest extends BaseGeneratorCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:request {name} {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Request';

    protected $type = 'Request'; // shows up in console

    protected string|null $rootNamespace = 'Domain\Requests';

    protected string|null $prefixName = 'Request';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return base_path('stubs/request.stub');
    }
}
