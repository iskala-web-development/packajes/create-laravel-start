<?php
declare(strict_types=1);
namespace App\Exceptions;

use App\Exceptions\ResponderExceptions\ResourceNotFound;
use Exception;
use Flugg\Responder\Contracts\Responder;
use Flugg\Responder\Exceptions\Http\HttpException;
use Flugg\Responder\Exceptions\Http\PageNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;
use Illuminate\Foundation\Configuration\Exceptions;

class ResponderException
{
    use \Flugg\Responder\Exceptions\ConvertsExceptions;

    protected function convertResponderException($exception): void
    {
        $this->convert($exception, array_diff_key([
            PageNotFoundException::class => ResourceNotFound::class,
        ], array_flip($this->dontConvert)));
    }

//    private function convertExceptionToArray(Throwable $e): array|null
//    {
//        return config('app.debug') ? [
//            'message' => $e->getMessage(),
//            'exception' => get_class($e),
//            'file' => $e->getFile(),
//            'line' => $e->getLine(),
//            'trace' => collect($e->getTrace())->map(fn ($trace) => Arr::except($trace, ['args']))->all(),
//        ] : null;
//    }


    // TODO: решить, нужна ли полная надстройка над ошибками

    /**
     * @param Request $request
     * @param Exception $exception
     * @return JsonResponse|bool
     */
    public function render(Request $request, Exception $exception): JsonResponse|false
    {
        if (!$request->wantsJson())
            return false;

        $this->convertDefaultException($exception);
        $this->convertResponderException($exception);

        if($exception instanceof HttpException)
            return $this->renderResponse($exception);
//        else
//            return app(Responder::class)
//                ->error('critical_error')
//                ->data($this->convertExceptionToArray($exception))
//                ->respond(500);
        return false;
    }
}
