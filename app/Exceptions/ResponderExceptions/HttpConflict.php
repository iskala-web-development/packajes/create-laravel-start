<?php
declare(strict_types=1);
namespace App\Exceptions\ResponderExceptions;

use Flugg\Responder\Exceptions\Http\HttpException;

class HttpConflict extends HttpException
{
    /**
     * An HTTP status code.
     *
     * @var int
     */
    protected $status = 409;

    /**
     * An error code.
     *
     * @var string|null
     */
    protected $errorCode = 'resource_conflict';
}
