<?php
declare(strict_types=1);
namespace App\Exceptions\ResponderExceptions;

use Flugg\Responder\Exceptions\Http\HttpException;

class HttpCriticalError extends HttpException
{
    /**
     * An HTTP status code.
     *
     * @var int
     */
    protected $status = 500;

    /**
     * An error code.
     *
     * @var string|null
     */
    protected $errorCode = 'critical_error';
}
