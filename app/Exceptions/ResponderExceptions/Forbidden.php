<?php
declare(strict_types=1);
namespace App\Exceptions\ResponderExceptions;

use Flugg\Responder\Exceptions\Http\HttpException;

class Forbidden extends HttpException
{
    /**
     * An HTTP status code.
     *
     * @var int
     */
    protected $status = 403;

    /**
     * An error code.
     *
     * @var string|null
     */
    protected $errorCode = 'resource_forbidden';
}
