<?php
declare(strict_types=1);
namespace App\Responders;

use App\Actions\BaseAction;
use App\Interfaces\ResponderInterface;
use Flugg\Responder\Http\Responses\ErrorResponseBuilder;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseHttp;

abstract class BaseResponder extends ResponseHttp implements ResponderInterface
{
    /**
     * @var BaseAction|null $action
     */
    protected BaseAction|null $action = null;

    /**
     * @param BaseAction $action
     * @return $this
     */
    public function __invoke(BaseAction $action): static
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return int|null
     */
    protected function generateStatus(): ?int
    {
        $errorCode = $this->action->getErrorCode();

        return match($errorCode){
            'critical_error' => 500,
            'unauthenticated', 'unauthorized' => 401,
            'page_not_found' => 404,
            'resource_not_permissions' => 403,
            'record_conflict', 'record_exist' => 409,
            'validation_failed' => 422,
            default => $this->action->getStatus(),
        };
    }

    protected function isFail(): bool
    {
        return $this->action->isFail();
    }

    /**
     * @param mixed|null $data
     * @param callable|Transformer|string|null $transformer
     * @param string|null $resourceKey
     * @return SuccessResponseBuilder
     */
    protected function success(mixed $data = null, callable|Transformer|null|string $transformer = null, null|string $resourceKey = null): SuccessResponseBuilder
    {
        $transformData = match ($data) {
            is_bool($data) || is_int($data) => ['status' => $data],
            is_string($data) => ['message' => $data],
            default => $data,
        };

        return responder()->success($transformData, $transformer, $resourceKey);
    }

    /**
     * Build an error response with action error.
     *
     * @param string|null $code
     * @param string|null $message
     * @return ErrorResponseBuilder
     */
    protected function error(string|null $code = null, string|null $message = null): ErrorResponseBuilder
    {
        $actionData = $this->action->getErrorData();
        $data = $actionData ? ['data' => $actionData] : null;
        return responder()->error($code ?? $this->action->getErrorCode(), $message ?? $this->action->getErrorMessage())->data($data);
    }
}
