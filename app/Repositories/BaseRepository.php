<?php
declare(strict_types=1);
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\Chat\Domain\Models\ChatMessage;

abstract class BaseRepository
{
    protected mixed $model = null;
    protected mixed $transformer = null;

    public function setModel(mixed $model): void
    {
        $this->model = $model;
    }

    public function transformData(mixed $Model): array
    {
        return $this->transformer->transform($Model);
    }

}
