<?php

namespace App\Enums;

enum EventType: string
{
    case Online = 'online';
    case Offline = 'offline';
}
