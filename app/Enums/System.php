<?php

namespace App\Enums;

enum System: string
{
    case WEB = 'web';
    case MOBILE = 'mobile';
}
