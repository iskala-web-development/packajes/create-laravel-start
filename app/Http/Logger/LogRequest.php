<?php
declare(strict_types=1);
namespace App\Http\Logger;

use Illuminate\Http\Request;
use Spatie\HttpLogger\LogProfile;

class LogRequest implements LogProfile
{

    public function shouldLogRequest(Request $request): bool
    {
        return in_array(strtolower($request->method()), ['get', 'post', 'put', 'patch', 'delete']);
    }
}
