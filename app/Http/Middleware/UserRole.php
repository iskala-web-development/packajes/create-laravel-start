<?php

namespace App\Http\Middleware;

use App\Exceptions\ResponderExceptions\ResourceNotPermissions;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string $role): Response
    {
        if(!Auth::check() || Auth::user()->role->value !== $role)
            throw new ResourceNotPermissions();


        return $next($request);
    }
}
