<?php

declare(strict_types=1);

namespace App\Services\Api;

use App\Services\Api\Traits\ApiConvert;
use App\Services\Api\Traits\ApiValidations;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use JetBrains\PhpStorm\ExpectedValues;

abstract class ApiService
{
    use ApiConvert, ApiValidations;

    protected ?string $url = '';

    protected ?string $urlPrefix = '';

    const int TOKEN_EXPIRE = 86400;

    const int LIMIT_REPEAT_SEND = 5;

    private int $repeat = 0;

    protected bool $generateUrlOff = false;

    protected \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response|null $response = null;

    protected int $countSend = 0;

    #[ExpectedValues(['success', 'warning', 'error'])]
    protected ?string $type = null;

    protected bool $status = true;

    protected ?array $successData = null;

    protected ?string $successMessage = null;

    protected ?array $warningData = null;

    protected ?string $warningMessage = null;

    protected ?array $errorData = null;

    protected ?string $errorMessage = null;

    protected ?string $errorCode = null;

    protected function generateUrl(string $api): array|string
    {
        $url = $this->url;

        if ($this->urlPrefix) {
            $url .= "/$this->urlPrefix";
        }

        return "$url/$api";
    }

    protected function generateHeader(): array
    {
        return [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
    }

    protected function generateParams(array $params = []): array
    {
        return array_merge([
            // Global Parameters
        ], array_diff($params, ['', null, false]));
    }

    /**
     * @throws ConnectionException
     */
    protected function send(string $api, array $bodyParams = [], #[ExpectedValues(['get', 'post', 'put'])] string $method = 'get'): bool|array|null
    {
        $i = 0;

        do {
            $url = ! $this->generateUrlOff ? $this->generateUrl($api) : $api;
            $http = Http::withHeaders($this->generateHeader());
            $params = $this->generateParams($bodyParams);

            $this->checkRequest();

            if ($method === 'get') {
                $this->response = $http->get($url, $params);
            } elseif ($method === 'post') {
                $this->response = $http->asForm()->post($url, $params);
            } elseif ($method === 'put') {
                $this->response = $http->asForm()->put($url, $params);
            }

            $this->validateResponse();
            $this->convertResponse();
            $this->validateError();

            $this->countSend++;

            $i++;
        } while ($i <= $this->repeat);

        return $this->status ? $this->successData : false;
    }

    abstract protected function checkRequest(): void;

    private function resend(): void
    {
        if ($this->repeat < self::LIMIT_REPEAT_SEND) {
            $this->repeat++;
        } else {
            $this->warningMessage = 'Было превышено максимальное кол-во повторов запроса';
        }
    }

    public function getSuccessData(): ?array
    {
        return $this->successData;
    }

    public function getSuccessMessage(): ?string
    {
        return $this->successMessage;
    }

    public function getWarningData(): ?array
    {
        return $this->warningData;
    }

    public function getWarningMessage(): string
    {
        return $this->warningMessage;
    }

    public function getErrorData(): ?array
    {
        return $this->errorData;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function getErrorStatus(): ?int
    {
        return $this->errorCode;
    }
}
