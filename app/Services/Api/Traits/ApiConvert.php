<?php

declare(strict_types=1);

namespace App\Services\Api\Traits;

trait ApiConvert
{
    private function convertResponse(): void
    {
        if ($this->response->successful()) {
            $this->convertSuccess();
        } else {
            $this->convertError();
        }
    }

    abstract protected function convertSuccess(): void;

    abstract protected function convertError(): void;
}
