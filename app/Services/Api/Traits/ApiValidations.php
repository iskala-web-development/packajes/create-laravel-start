<?php

declare(strict_types=1);

namespace App\Services\Api\Traits;

trait ApiValidations
{
    private function validateResponse(): void
    {
        if ($this->response->successful()) {
            $this->setSuccess();
        } else {
            $this->setError();
        }
    }

    protected function setSuccess(?string $message = null, array|string|null $data = null): void
    {
        $this->successData = $data ?? $this->response->json();
        $this->successMessage = $message;
        $this->type = 'success';
        $this->status = true;
    }

    protected function setError(?string $message = null, array|string|null $data = null): void
    {
        $this->errorData = $data ?? $this->response->json();
        $this->errorMessage = $message;
        $this->type = 'error';
        $this->status = false;
    }

    protected function setWarning(?string $message = null, array|string|null $data = null): void
    {
        $this->warningData = $data ?? $this->response->json();
        $this->warningMessage = $message;
        $this->status = true;
    }

    abstract protected function validateError(): void;
}
