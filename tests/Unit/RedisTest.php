<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class RedisTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_connect(): void
    {
        try {
            Redis::ping();
            // Connection successful
            $this->assertTrue(true, 'Connection success');

        } catch (\Exception $e) {
            // Connection failed
            $this->assertTrue(false, 'Connection failed');
        }
    }
}
