<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class DBTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_connect(): void
    {
        $this->assertTrue(DB::connection()->getPdo() != null);
    }
}
