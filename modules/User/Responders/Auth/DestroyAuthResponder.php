<?php
declare(strict_types=1);
namespace Modules\User\Responders\Auth;

use App\Responders\BaseResponder;
use Illuminate\Http\JsonResponse;

class DestroyAuthResponder extends BaseResponder
{
    /**
     * @param bool $data
     * @return JsonResponse
     */
    public function response(bool $data): JsonResponse
    {
        return $this->isFail()
            ? $this->error()->respond($this->generateStatus())
            : $this->success()->respond(static::HTTP_NO_CONTENT);
    }
}
