<?php
declare(strict_types=1);
namespace Modules\User\Responders\Auth;

use App\Responders\BaseResponder;
use Illuminate\Http\JsonResponse;
use Modules\User\Domain\Models\User;
use Modules\User\Domain\Transformers\AuthTransformer;

class LoginAuthResponder extends BaseResponder
{
    /**
     * @param User $user
     * @return JsonResponse
     */
    public function response(User $user): JsonResponse
    {
        return $this->isFail()
            ? $this->error()->respond($this->generateStatus())
            : $this->success($user, AuthTransformer::class)->respond();
    }
}
