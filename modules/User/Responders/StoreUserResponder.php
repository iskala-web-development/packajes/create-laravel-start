<?php
declare(strict_types=1);
namespace Modules\User\Responders;

use App\Responders\BaseResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class StoreUserResponder extends BaseResponder
{
    /**
     * @param mixed $data
     * @return JsonResponse
     */
    public function response(mixed $data = []): JsonResponse
    {
        return $this->isFail()
            ? $this->error()->respond($this->generateStatus())
            : $this->success($data)->respond(static::HTTP_CREATED);
    }
}
