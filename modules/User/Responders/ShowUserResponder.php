<?php
declare(strict_types=1);
namespace Modules\User\Responders;

use App\Responders\BaseResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Modules\User\Domain\Models\User;

class ShowUserResponder extends BaseResponder
{
    /**
     * @param User $user
     * @return JsonResponse
     */
    public function response(User $user): JsonResponse
    {
        return $this->isFail()
            ? $this->error()->respond($this->generateStatus())
            : $this->success($user)->respond();
    }
}
