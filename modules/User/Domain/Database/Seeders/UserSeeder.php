<?php
declare(strict_types=1);
namespace Modules\User\Domain\Database\Seeders;

use App\Enums\UserRole;
use Illuminate\Database\Seeder;
use Modules\User\Domain\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->state([
            'email' => 'example@doamin.com',
            'role' => UserRole::Admin,
        ])->create();

        User::factory()->count(50)->state([
            'role' => UserRole::User,
        ])->create();
    }
}
