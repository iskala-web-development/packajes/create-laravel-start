<?php
declare(strict_types=1);
namespace Modules\User\Domain\Transformers;

use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Facades\Auth;
use Modules\User\Domain\Models\User;

class AuthTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * A list of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param User $User
     * @return array
     */
    public function transform(User $User): array
    {
        return [
            'user' => $User->toArray(),
            'api_token' => $User->getApiToken()
        ];
    }
}
