<?php
declare(strict_types=1);
namespace Modules\User\Domain\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\User\Actions\DestroyUserAction;
use Modules\User\Actions\IndexUserAction;
use Modules\User\Actions\ShowUserAction;
use Modules\User\Actions\StoreUserAction;
use Modules\User\Actions\UpdateUserAction;
use Modules\User\Domain\Requests\IndexUserRequest;
use Modules\User\Domain\Requests\StoreUserRequest;
use Modules\User\Domain\Requests\UpdateUserRequest;
use Modules\User\Responders\DestroyUserResponder;
use Modules\User\Responders\IndexUserResponder;
use Modules\User\Responders\ShowUserResponder;
use Modules\User\Responders\StoreUserResponder;
use Modules\User\Responders\UpdateUserResponder;

class UserController extends BaseController
{
    /**
     * @param IndexUserRequest $request
     * @param IndexUserAction $action
     * @param IndexUserResponder $responder
     * @return JsonResponse
     */
    public function index(IndexUserRequest $request, IndexUserAction $action, IndexUserResponder $responder): JsonResponse
    {
        $request->validated();

        return $responder($action)->response($action->handle($request));
    }

    /**
     * @param StoreUserRequest $request
     * @param StoreUserAction $action
     * @param StoreUserResponder $responder
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request, StoreUserAction $action, StoreUserResponder $responder): JsonResponse
    {
        $request->validated();

        return $responder($action)->response($action->handle($request));
    }

    /**
     * @param string $id
     * @param ShowUserAction $action
     * @param ShowUserResponder $responder
     * @return JsonResponse
     */
    public function show(string $id, ShowUserAction $action, ShowUserResponder $responder): JsonResponse
    {
        return $responder($action)->response($action->handle($id));
    }

    /**
     * @param UpdateUserRequest $request
     * @param string $id
     * @param UpdateUserAction $action
     * @param UpdateUserResponder $responder
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, string $id, UpdateUserAction $action, UpdateUserResponder $responder): JsonResponse
    {
        $request->validated();

        return $responder($action)->response($action->handle($request, $id));
    }

    /**
     * @param string $id
     * @param DestroyUserAction $action
     * @param DestroyUserResponder $responder
     * @return JsonResponse
     */
    public function destroy(string $id, DestroyUserAction $action, DestroyUserResponder $responder): JsonResponse
    {
        return $responder($action)->response($action->handle($id));
    }
}
