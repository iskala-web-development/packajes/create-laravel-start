<?php
declare(strict_types=1);
namespace Modules\User\Domain\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\User\Actions\Auth\DestroyAuthAction;
use Modules\User\Actions\Auth\LoginAuthAction;
use Modules\User\Actions\Auth\RegisterAuthAction;
use Modules\User\Domain\Requests\Auth\LoginAuthRequest;
use Modules\User\Domain\Requests\Auth\RegisterAuthRequest;
use Modules\User\Responders\Auth\DestroyAuthResponder;
use Modules\User\Responders\Auth\LoginAuthResponder;
use Modules\User\Responders\Auth\RegisterAuthResponder;

class AuthController extends BaseController
{
    /**
     * @param LoginAuthRequest $request
     * @param LoginAuthAction $action
     * @param LoginAuthResponder $responder
     * @return JsonResponse
     */
    public function login(LoginAuthRequest $request, LoginAuthAction $action, LoginAuthResponder $responder): JsonResponse
    {
        $request->validated();

        return $responder($action)->response($action->handle($request));
    }

    /**
     * @param RegisterAuthRequest $request
     * @param RegisterAuthAction $action
     * @param RegisterAuthResponder $responder
     * @return JsonResponse
     */
    public function register(RegisterAuthRequest $request, RegisterAuthAction $action, RegisterAuthResponder $responder): JsonResponse
    {
        $request->validated();

        return $responder($action)->response($action->handle($request));
    }

    /**
     * @param Request $request
     * @param DestroyAuthAction $action
     * @param DestroyAuthResponder $responder
     * @return JsonResponse
     */
    public function logout(Request $request, DestroyAuthAction $action, DestroyAuthResponder $responder): JsonResponse
    {
        return $responder($action)->response($action->handle($request));
    }
}
