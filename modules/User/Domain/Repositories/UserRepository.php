<?php
declare(strict_types=1);
namespace Modules\User\Domain\Repositories;

use App\Enums\UserRole;
use App\Repositories\BaseRepository;
use Modules\User\Domain\Models\User;

class UserRepository extends BaseRepository
{
    public function paginate(int $limit, string $sortKey, string $sortOrder): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return User::orderBy($sortKey, $sortOrder)->paginate($limit);
    }

    public function whereOrFailByEmail(string $email): User
    {
        return User::where('email', $email)->firstOrFail();
    }

    public function findOrFail(string|int $id): mixed
    {
        return User::findOrFail($id);
    }

    public function checkLastAdminRole(): bool
    {
        return User::where('role', UserRole::Admin)->count() > 1;
    }

    public function create(array $data): User
    {
        return User::create($data);
    }

    public function createWithRole(array $data, UserRole $role): User
    {
        return $this->create([...$data, 'role' => $role]);
    }

    public function update(User $user, array $data): bool
    {
        return $user->update($data);
    }

    public function delete(User $user): bool
    {
        return $user->delete();
    }
}
