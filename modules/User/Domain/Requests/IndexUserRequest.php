<?php
declare(strict_types=1);
namespace Modules\User\Domain\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'limit' => ['string'],
            'page' => ['string'],
            'sort_key' => ['string'],
            'sort_order' => ['string', 'in:asc,desc'],
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
