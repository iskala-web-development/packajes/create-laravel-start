<?php
declare(strict_types=1);
namespace Modules\User\Domain\Requests\Auth;

use App\Enums\System;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class RegisterAuthRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'unique:users', 'max:255'],
            'password' => ['required', 'string', 'max:32', 'min:6', 'confirmed'],
            'system' => ['required', 'string', new Enum(System::class)]
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
