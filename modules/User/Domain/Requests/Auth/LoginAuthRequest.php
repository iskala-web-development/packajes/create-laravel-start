<?php
declare(strict_types=1);
namespace Modules\User\Domain\Requests\Auth;

use App\Enums\System;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class LoginAuthRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'exists:users'],
            'password' => ['required', 'string'],
            'system' => ['required', 'string', new Enum(System::class)]
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
