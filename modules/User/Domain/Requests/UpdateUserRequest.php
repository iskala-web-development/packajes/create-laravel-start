<?php
declare(strict_types=1);
namespace Modules\User\Domain\Requests;

use App\Enums\UserRole;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', Rule::unique('users')->ignore($this->id)],
            'password' => ['nullable','string', 'max:32', 'min:6'],
            'role' => ['required', 'string', new Enum(UserRole::class)]
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
