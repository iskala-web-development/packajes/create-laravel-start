<?php
declare(strict_types=1);
namespace Modules\User\Actions\Auth;

use App\Actions\BaseAction;
use Illuminate\Http\Request;

class DestroyAuthAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct()
    {
        // $this->repository = $repository;
        // $this->service = $service;
    }

    public function handle(Request $request): bool
    {
        return $request->user()->currentAccessToken()->delete();
    }
}
