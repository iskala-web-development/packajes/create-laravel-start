<?php
declare(strict_types=1);
namespace Modules\User\Actions\Auth;

use App\Actions\BaseAction;
use App\Enums\UserRole;
use Modules\User\Domain\Models\User;
use Modules\User\Domain\Repositories\UserRepository;
use Modules\User\Domain\Requests\Auth\RegisterAuthRequest;

class RegisterAuthAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        // $this->service = $service;
    }

    public function handle(RegisterAuthRequest $request): User
    {
        $user = $this->repository->createWithRole($request->validationData(), UserRole::User);

        $user->auth($request->validated('system'));

        return $user;
    }
}
