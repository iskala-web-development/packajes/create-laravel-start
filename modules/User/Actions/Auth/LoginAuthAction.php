<?php
declare(strict_types=1);
namespace Modules\User\Actions\Auth;

use App\Actions\BaseAction;
use Illuminate\Support\Facades\Hash;
use Modules\User\Domain\Models\User;
use Modules\User\Domain\Repositories\UserRepository;
use Modules\User\Domain\Requests\Auth\LoginAuthRequest;

class LoginAuthAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        // $this->service = $service;
    }

    public function handle(LoginAuthRequest $request): false|User
    {
        $user = $this->repository->whereOrFailByEmail($request->validated('email'));

        if(!Hash::check($request->validated('password'), $user->password))
            return $this->error(static::PASSWORD_FAILED);

        $user->auth($request->validated('system'));

        return $user;
    }
}
