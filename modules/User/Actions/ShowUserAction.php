<?php
declare(strict_types=1);
namespace Modules\User\Actions;

use App\Actions\BaseAction;
use Illuminate\Support\Collection;
use Modules\User\Domain\Repositories\UserRepository;

class ShowUserAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        // $this->service = $service;
    }

    public function handle(string $id)
    {
        return $this->repository->findOrFail($id);
    }
}
