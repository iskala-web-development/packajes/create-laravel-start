<?php
declare(strict_types=1);
namespace Modules\User\Actions;

use App\Actions\BaseAction;
use Illuminate\Support\Collection;
use Modules\User\Domain\Repositories\UserRepository;
use Modules\User\Domain\Requests\StoreUserRequest;

class StoreUserAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        // $this->service = $service;
    }

    public function handle(StoreUserRequest $request)
    {
        return $this->repository->create($request->validationData());
    }
}
