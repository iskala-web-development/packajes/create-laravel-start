<?php
declare(strict_types=1);
namespace Modules\User\Actions;

use App\Actions\BaseAction;
use App\Enums\UserRole;
use Illuminate\Support\Collection;
use Modules\User\Domain\Repositories\UserRepository;
use Modules\User\Domain\Requests\UpdateUserRequest;

class UpdateUserAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(UpdateUserRequest $request, string $id): bool
    {
        $user = $this->repository->findOrFail($id);

        if($request->validated('role') !== UserRole::Admin->value && $user->role !== $request->validated('role') && !$this->repository->checkLastAdminRole())
            return $this->error(static::RECORD_CONFLICT, 'Вы не можете изменить роль администратора, по скольку вы последний администратор');

        return $this->repository->update($user, $request->validationData());
    }
}
