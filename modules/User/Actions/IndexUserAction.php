<?php
declare(strict_types=1);
namespace Modules\User\Actions;

use App\Actions\BaseAction;
use Modules\User\Domain\Repositories\UserRepository;
use Modules\User\Domain\Requests\IndexUserRequest;

class IndexUserAction extends BaseAction
{
    /**
     * @var $repository - TODO: Write ClassName for help IDE
     */
    protected mixed $repository;

    /**
     * @var $service - TODO: Write ClassName for help IDE
     */
    protected mixed $service;

    /**
     * TODO: Register Repository and Service
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(IndexUserRequest $request): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return $this->repository->paginate(
            $request->integer('limit', 10),
            $request->validated('sort_key', 'id'),
            $request->validated('sort_order', 'asc')
        );
    }
}
