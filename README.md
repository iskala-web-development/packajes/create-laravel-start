<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

### INIT PROJECT
```bash
$ composer install
$ cp .env.example .env
$ # Ввести данные от базы данных и redis
$ php artisan key:generate
$ php artisan optimize
$ php artisan migrate
```

### START SERVER
```bash
$ php artisan serve
```

### ARTISAN CUSTOM COMMANDS
```bash
$ php artisan module:controller ${name} ${moduleName}
$ php artisan module:action ${name} ${moduleName}
$ php artisan module:responder ${name} ${moduleName}
$ php artisan module:model ${name} ${moduleName}
$ php artisan module:migration ${name} ${moduleName}
$ php artisan module:factory ${name} ${moduleName}
$ php artisan module:seeder ${name} ${moduleName}
$ php artisan make:provider ${name}
$ php artisan make:command ${name} # Create Command
$ php artisan schedule:list # List Schedules
```

### LARAVEL IDEA HELP
#### Init Help
```bash
$ php artisan ide-helper:generate
```

#### Generate Model Help
```bash
$ php artisan ide-helper:models
```


### XDEBUG
```bash
$ export PHP_IDE_CONFIG="serverName=laravel"
```

### PHPSTORM
```dotenv
PHP_IDE_CONFIG=serverName=laravel
```
> Добавить в настройки терминала проекта
