<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return [
        'Laravel version' => app()->version(), 'System version' => system_version(),
    ];
});
