<?php

declare(strict_types=1);
use Illuminate\Http\JsonResponse;

if(! function_exists('system_version')){
    function system_version(): string|null
    {
        $package = \Composer\InstalledVersions::getRootPackage();

        return $package['version'];
    }
}

if (! function_exists('resp')) {
    function resp(mixed $data, ?string $message = null, int $status = 200, array $obj = []): JsonResponse
    {
        return response()->json(array_filter(['message' => $message, 'data' => $data, ...$obj]), $status);
    }
}

if (! function_exists('convert_bit')) {
    function convert_bit(int $bit): array
    {
        // Байты в Мегабайты
        $res = round($bit / 1048576, 2, PHP_ROUND_HALF_DOWN);
        $unit = 'МБ';

        // Мегабайты в Гигабайты
        if ($res >= 1024) {
            $res = round($res / 1024, 2, PHP_ROUND_HALF_DOWN);
            $unit = 'ГБ';
        }

        // Гигабайты в Терабайты
        if ($res >= 1024) {
            $res = round($res / 1024, 2, PHP_ROUND_HALF_DOWN);
            $unit = 'ТБ';
        }

        return ['value' => $res, 'unit' => $unit];
    }
}

if(!function_exists('app_log')){
    function app_log(): AppLog
    {
        class AppLog
        {
            public function service(string $name, string|null $logName = null): \Psr\Log\LoggerInterface
            {
                $logName = $logName ?? $name;

                return Log::build([
                    'driver' => 'daily',
                    'path' => storage_path("logs/services/$name/".date('Y-m-d')."/$logName.log"),
                    'days' => 7
                ]);
            }
        }

        return new AppLog();
    }
}
