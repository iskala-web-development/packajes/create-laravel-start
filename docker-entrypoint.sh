#!/bin/sh

# Install composer
composer install --no-cache --ignore-platform-reqs

# Production artisan commands
if [ "$APP_ENV" != "production" ]; then php artisan migrate:rollback; fi
if [ "$APP_ENV" != "production" ]; then php artisan migrate; fi
if [ "$APP_ENV" != "production" ]; then php artisan db:seed; fi

# Artisan commands
php artisan optimize

exec "$@"
